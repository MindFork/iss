<?php

namespace MindFork\Controllers;

use MindFork\Core\Config;
use MindFork\Core\Controller;
use MindFork\Core\Template;
use MindFork\Core\Views\HtmlView;
use MindFork\Models\Dto\GoogleMapsApiDto;
use MindFork\Models\Dto\IssApiDto;
use MindFork\Models\GoogleLocation;
use MindFork\Models\Satellite;

final class IssController extends Controller
{
    public function __construct(Config $config)
    {
        parent::__construct($config);
        $this->config->loadFile('satellite_api');
        $this->config->loadFile('google_maps_api');
    }

    public function index()
    {
        $satellite = new Satellite(new IssApiDto($this->getConfig()));
        $satelliteLocation = $satellite->getLocation();

        $googleLocation = new GoogleLocation(new GoogleMapsApiDto($this->getConfig()));
        $googleLocation->setLatitude($satelliteLocation->getLatitude());
        $googleLocation->setLongitude($satelliteLocation->getLongitude());
        $googleLocation->updateReversedGeocodingData();
        $reversedGeocodingData = $googleLocation->getReversedGeocodingData();

        if(empty($reversedGeocodingData->results)) {
            $formattedAddress = "Unknown address, perhaps over some distant sea... :)";
        } else {
            $formattedAddress = $reversedGeocodingData->results[0]->formatted_address;
        }

        $view = new HtmlView();
        $view->render(new Template('index.php'), [
            'formattedAddress' => $formattedAddress,
            'latitude' => $satelliteLocation->getLatitude(),
            'longitude' => $satelliteLocation->getLongitude()
        ]);
    }
}