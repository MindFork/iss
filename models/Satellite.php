<?php

namespace MindFork\Models;

use MindFork\Models\Dto\IssApiDto;

final class Satellite
{
    private $name;

    private $id;

    private $location;

    public function __construct(IssApiDto $apiDao)
    {
        $this->id = $apiDao->getParameterByName('id');
        $this->name = $apiDao->getParameterByName('name');
        $this->location = new SatelliteLocation($apiDao);
    }

    public function getLocation(): SatelliteLocation
    {
        return $this->location;
    }

    public function reloadLocation()
    {
        $this->location->updateLocationData();
    }
}