<?php

namespace MindFork\Models\Interfaces;

interface GeoLocationInterface
{
    public function getLatitude();
    public function getLongitude();
}