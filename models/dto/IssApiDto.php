<?php

namespace MindFork\Models\Dto;

use MindFork\Core\Config;
use MindFork\Models\Exceptions\MissingConfigurationException;

final class IssApiDto
{
    private $config;

    private $data;

    public function __construct(Config $config)
    {
        $this->config = $config;
        if(empty($this->config->getItem('iss_location_api_url')) || empty($this->config->getItem('iss_satellite_id'))) {
            throw new MissingConfigurationException();
        }
        $this->data = $this->fetchLatestDataFromApi();
    }

    public function loadIssData()
    {
        $this->data = $this->fetchLatestDataFromApi();
    }

    public function getIssData()
    {
        if(empty($this->data)) {
            $this->loadIssData();
        }
        return $this->data;
    }

    private function fetchLatestDataFromApi()
    {
        return json_decode(
            file_get_contents(
                $this->config->getItem('iss_location_api_url') .
                $this->config->getItem('iss_satellite_id')
            )
        );
    }

    public function getParameterByName($name)
    {
        return !empty($this->data->$name) ? $this->data->$name : null;
    }

}