<?php

namespace MindFork\Models\Dto;

use MindFork\Core\Config;

final class GoogleMapsApiDto
{
    private $latitude;

    private $longitude;

    private $reversedGeocodingData;

    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function loadReversedGeocodingData()
    {
        $url = $this->config->getItem('google_reverse_geocoding_url');
        $url = str_replace('_LATITUDE_', $this->latitude, $url);
        $url = str_replace('_LONGITUDE_', $this->longitude, $url);
        $url = str_replace('_API_KEY_', $this->config->getItem('google_maps_api_key'), $url);
        $this->reversedGeocodingData = json_decode(file_get_contents($url));
    }

    public function getReversedGeocodingData()
    {
        return $this->reversedGeocodingData;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }
    
    
}