<?php

namespace MindFork\Models;

use MindFork\Models\Dto\GoogleMapsApiDto;
use MindFork\Models\Interfaces\GeoLocationInterface;

final class GoogleLocation implements GeoLocationInterface
{
    private $latitude;

    private $longitude;

    private $reversedGeocodingData;

    private $googleMapsApiDto;

    public function __construct(GoogleMapsApiDto $googleMapsApiDto)
    {
        $this->googleMapsApiDto = $googleMapsApiDto;
    }

    public function updateReversedGeocodingData()
    {
        $this->googleMapsApiDto->setLatitude($this->latitude);
        $this->googleMapsApiDto->setLongitude($this->longitude);
        $this->googleMapsApiDto->loadReversedGeocodingData();
        $this->reversedGeocodingData = $this->googleMapsApiDto->getReversedGeocodingData();
    }

    public function getReversedGeocodingData()
    {
        return $this->reversedGeocodingData;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }


}