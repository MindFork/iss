<?php

namespace MindFork\Models;

use MindFork\Models\Dto\IssApiDto;
use MindFork\Models\Interfaces\GeoLocationInterface;

final class SatelliteLocation implements GeoLocationInterface
{
    private $latitude;

    private $longitude;

    private $altitude;

    private $velocity;

    private $units;

    private $timestamp;

    private $apiDto;

    public function __construct(IssApiDto $apiDto)
    {
        $this->apiDto = $apiDto;
        $this->updateLocationData();
    }

    public function updateLocationData()
    {
        $this->apiDto->loadIssData();
        $this->latitude = $this->apiDto->getParameterByName('latitude');
        $this->longitude = $this->apiDto->getParameterByName('longitude');
        $this->altitude = $this->apiDto->getParameterByName('altitude');
        $this->velocity = $this->apiDto->getParameterByName('velocity');
        $this->units = $this->apiDto->getParameterByName('units');
        $this->timestamp = $this->apiDto->getParameterByName('timestamp');
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * @param mixed $altitude
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;
    }

    /**
     * @return mixed
     */
    public function getVelocity()
    {
        return $this->velocity;
    }

    /**
     * @param mixed $velocity
     */
    public function setVelocity($velocity)
    {
        $this->velocity = $velocity;
    }

    /**
     * @return mixed
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param mixed $units
     */
    public function setUnits($units)
    {
        $this->units = $units;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }
}