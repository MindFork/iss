<?php
namespace MindFork\Models\Exceptions;

class MissingConfigurationException extends \Exception {}