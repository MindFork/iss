<?php

namespace MindFork\Core;

use MindFork\Core\Interfaces\RendererInterface;

abstract class View implements RendererInterface {}