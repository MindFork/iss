<?php

namespace MindFork\Core\Interfaces;

use MindFork\Core\Template;

interface RendererInterface
{
    public function render(Template $template, Array $data = null);
}