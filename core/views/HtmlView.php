<?php

namespace MindFork\Core\Views;

use MindFork\Core\Template;
use MindFork\Core\View;

final class HtmlView extends View
{
    public function render(Template $template, Array $data = null)
    {
        foreach ($data as $variable => $value) {
            $$variable = $value;
        }
        include( APP_PATH . 'views' . DIRECTORY_SEPARATOR . $template->getPath());
    }
}