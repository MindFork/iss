<?php

namespace MindFork\Core;

final class Config
{
    private $config;

    public function __construct($fileNames = [])
    {
        $this->config = [];
        foreach ($fileNames as $fileName) {
            $this->loadFile($fileName);
        }
    }

    public function loadFile($fileName)
    {
        $this->config = array_merge(
            $this->config,
            json_decode(
                file_get_contents(
                    APP_PATH .
                    DIRECTORY_SEPARATOR .
                    'config' .
                    DIRECTORY_SEPARATOR .
                    $fileName . '.json'
                ),true
            )
        );
    }

    public function getItem($key)
    {
        return array_key_exists($key, $this->config) ? $this->config[$key] : null;
    }



}