<?php

namespace MindFork\Core;

abstract class Controller {

    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function getConfig()
    {
        return $this->config;
    }

}