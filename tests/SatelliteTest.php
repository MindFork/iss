<?php
declare(strict_types=1);
namespace MindFork\Tests;

use MindFork\Core\Config;
use MindFork\Models\Dto\IssApiDto;
use MindFork\Models\Exceptions\MissingConfigurationException;
use MindFork\Models\Satellite;
use PHPUnit\Framework\TestCase;

/**
 * @covers Satellite
 */
final class SatelliteTest extends TestCase
{
    public function testCreateProperSatelliteInstance(): void
    {
        $this->assertInstanceOf(
            Satellite::class,
            new Satellite(new IssApiDto(new Config([
                'satellite_api',
                'google_maps_api'
            ])))
        );
    }

    public function testFailSatelliteCreationDueToInvalidConfig(): void
    {
        try {
            new Satellite(new IssApiDto(new Config([])));
        } catch (MissingConfigurationException $e) {
            $this->assertInstanceOf(
                MissingConfigurationException::class,
                $e
            );
        }
    }

    public function testCreateSatelliteAndGetItsCoordinates(): void
    {
        $satellite = new Satellite(new IssApiDto(new Config([
            'satellite_api',
            'google_maps_api'
        ])));
        $satelliteLocation = $satellite->getLocation();
        $latitude = $satelliteLocation->getLongitude();
        $longitude = $satelliteLocation->getLongitude();
        $this->assertInternalType('float', $latitude);
        $this->assertInternalType('float', $longitude);
        $this->assertGreaterThanOrEqual(-180, $latitude);
        $this->assertLessThanOrEqual(180, $latitude);
        $this->assertGreaterThanOrEqual(-90, $longitude);
        $this->assertLessThanOrEqual(90, $longitude);
    }

    public function testSatelliteLocationUpdate()
    {
        $satellite = new Satellite(new IssApiDto(new Config([
            'satellite_api',
            'google_maps_api'
        ])));
        $satelliteLocation = $satellite->getLocation();
        $initialLatitude = $satelliteLocation->getLatitude();
        $initialLongitude = $satelliteLocation->getLongitude();

        $this->assertInternalType('float', $initialLatitude);
        $this->assertInternalType('float', $initialLongitude);
        $this->assertGreaterThanOrEqual(-180, $initialLatitude);
        $this->assertLessThanOrEqual(180, $initialLatitude);
        $this->assertGreaterThanOrEqual(-90, $initialLongitude);
        $this->assertLessThanOrEqual(90, $initialLongitude);

        $satelliteLocation->updateLocationData();

        $updatedLatitude = $satelliteLocation->getLongitude();
        $updatedLongitude = $satelliteLocation->getLongitude();

        $this->assertInternalType('float', $updatedLatitude);
        $this->assertInternalType('float', $updatedLongitude);
        $this->assertGreaterThanOrEqual(-180, $updatedLatitude);
        $this->assertLessThanOrEqual(180, $updatedLatitude);
        $this->assertGreaterThanOrEqual(-90, $updatedLongitude);
        $this->assertLessThanOrEqual(90, $updatedLongitude);

        $this->assertNotEquals($initialLatitude, $updatedLatitude);
        $this->assertNotEquals($initialLongitude, $updatedLongitude);
    }



}