<?php

namespace MindFork;

use MindFork\Controllers\IssController;
use MindFork\Core\Config;

define('APP_PATH', dirname(__FILE__) . '/../');

require APP_PATH . 'vendor/autoload.php';

$controller = new IssController(new Config());
$controller->index();
